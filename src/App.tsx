import React from 'react';
import  { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Header } from './components/Header';
import { SignIn } from './pages/SignIn';
import { SignUp } from './pages/SignUp';
import { Profile } from './pages/Profile';
import { Articles } from './pages/Articles';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Header/>
      <div className='container'>  
        <Switch>
          <Route component={Articles} path='/' exact />
          <Route component={SignIn} path='/sign-in' />
          <Route component={SignUp} path='/sign-up' />
          <Route component={Profile} path='/profile' />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
