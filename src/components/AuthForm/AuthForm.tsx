import React, { useState } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { AppState } from '../../store/store';
import { validateEmail, validatePassword } from './validation';
import { Button } from '../Button';
import './AuthForm.scss';

interface IParentProps {
  onSuccessSubmit: (email: string, password: string) => void
}
interface IStateProps {
  errorMessage?: string | null
}

type IAuthFormProps = IParentProps & IStateProps; 

const AuthFormComponent: React.FC<IAuthFormProps> = (props: IAuthFormProps) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [emailError, setEmailError] = useState<boolean>(false);
  const [passwordError, setPasswordError] = useState<boolean>(false);
  const { errorMessage } = props;

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
    setEmailError(false);
    setEmail(event.target.value);
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
    setPasswordError(false);
    setPassword(event.target.value);
  }

  const validateForm = ():void => {
    if (!validateEmail(email)) setEmailError(true);
    if (!validatePassword(password)) setPasswordError(true);
  }

  const handleFormSubmit = (event: React.FormEvent<HTMLFormElement>):void => {
    event.preventDefault();
    validateForm();
    if (!emailError && !passwordError) {
      props.onSuccessSubmit(email, password);
    }
  }


  return (
    <form className='auth-form' onSubmit={handleFormSubmit}>
      <div className={classnames("auth-form__field", {"auth-form__field--error": emailError})}>
        <label htmlFor="email">Email</label>
        <input type="text" name="email" id="email" placeholder="Email" value={email} onChange={handleEmailChange} />
        {emailError && <p className="auth-form__error">Wrong email format.</p>}
      </div>
      <div className={classnames("auth-form__field", {"auth-form__field--error": passwordError})}>
        <label htmlFor="email">Password</label>
        <input type="password" name="password" id="password" placeholder="Password" value={password} onChange={handlePasswordChange} />
        {passwordError && <p className="auth-form__error">Password should contain at least one capital character, one lower character, one digit character and minimum 8 characters.</p>}
      </div>
      <div className="auth-form__submit">
        <Button label="Submit" type="submit"></Button>
      </div>
      {errorMessage && 
        <p className="auth-form__error auth-form__error--centered">{errorMessage}</p>
      }
    </form>
  );
}

const mapState = (state: AppState): IStateProps => ({
  errorMessage: state.auth.authError
})

export const AuthForm: React.FC<IAuthFormProps> = connect(mapState)(AuthFormComponent);