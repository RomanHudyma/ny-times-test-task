export const validateEmail = (email: string):boolean => {
  const emailRegExp: RegExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  return emailRegExp.test(email);
}

export const validatePassword = (password: string):boolean => {
  const passRegExp: RegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
  return passRegExp.test(password);
}