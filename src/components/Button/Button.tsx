import React from 'react';
import './Button.scss';

interface IButtonProps {
  label: string,
  type?: "submit" | "button"
}

export const Button: React.FC<IButtonProps> = (props: IButtonProps) => {
  return (
    <button type={props.type || "button"} className="button">
      {props.label}
    </button>
  );
}

