import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from '../Button';
import './ErrorModal.scss';

interface IErrorModalProps {
  onClick: () => void
}

export const ErrorModal: React.FC<IErrorModalProps> = (props: IErrorModalProps) => {
  const { onClick } = props; 
  const handleModalClick = (event: React.MouseEvent) => {
    event.stopPropagation();
  }

  return (
    <div className="error-modal__overlay" onClick={onClick}>
      <div className="error-modal" onClick={handleModalClick}>
        <div className="error-modal__close"><div onClick={onClick}>×</div></div>
        <p>Please, sign in to see the whole article!</p>
        <NavLink to='/sign-in'><Button label='Go to Sign In' /></NavLink>
      </div>
    </div>
  );
}
