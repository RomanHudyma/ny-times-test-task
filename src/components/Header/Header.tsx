import React, { useState } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { ThunkDispatch } from 'redux-thunk';
import { Button } from '../Button';
import { UserPhoto } from '../UserPhoto';
import { AppState } from '../../store/store';
import { AppActions } from '../../store/actionTypes/actionTypes';
import { signOut } from '../../store/actions/authActions';
import './Header.scss';

interface IParentProps {}
interface IStateProps {
  isLogged?: boolean
}
interface IDispatchProps {
  signOut?: () => void
}

type IHeaderProps = IParentProps & IStateProps & IDispatchProps;

const HeaderComponent: React.FC<IHeaderProps> = (props: IHeaderProps) => {
  const { isLogged, signOut } = props;
  const [ showMenu, setShowMenu ] = useState<boolean>(false); 

  const handleDropdownClick = (event: React.MouseEvent):void => {
    setShowMenu(!showMenu);
  }

  const handleDropdownMouseLeave = (event: React.MouseEvent):void => {
    setShowMenu(false);
  }

  const handleLogoutClick = (event: React.MouseEvent):void => {
    event.preventDefault();
    signOut!();
  }

  return (
    <header className="header">
      <div className="container">
        <div className="header__inner">
          <NavLink to='/'>
            <h2 className="header__logo">NY Times App</h2>
          </NavLink>
          {!isLogged && 
            <NavLink to='/sign-in'>
              <Button label='Sign In'/>
            </NavLink>
          }
          {isLogged && 
            <div className="header__dropdown" onClick={handleDropdownClick}>
              <UserPhoto />
              {showMenu &&
                <ul className="header__submenu" onMouseLeave={handleDropdownMouseLeave}>
                  <li><NavLink to="/profile">Profile</NavLink></li>
                  <li><button onClick={handleLogoutClick}>Sign Out</button></li>
                </ul>
              }
            </div>
          }
        </div>
      </div>
    </header>
  );
}

const mapState = (state: AppState): IStateProps => ({
  isLogged: !state.firebase.auth.isEmpty
})

const mapDispatch = (dispatch: ThunkDispatch<AppState, {}, AppActions>): IDispatchProps => {
  return {
    signOut: () => {dispatch(signOut())}
  };
}

export const Header: React.FC<IHeaderProps> = connect(mapState, mapDispatch)(HeaderComponent);