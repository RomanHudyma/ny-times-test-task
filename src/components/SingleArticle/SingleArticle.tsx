import React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../../store/store';
import './SingleArticle.scss';

interface IParentProps {
  data: {[key: string]: any},
  onClick: () => void
}

interface IStateProps {
  isLogged?: boolean
}

interface IArticleWrapperProps {
  url: string,
  isLogged: boolean,
  onClick: () => void,
  children: React.ReactNode
}

type IArticlesProps = IParentProps & IStateProps;

const ArticleWrapper: React.FC<IArticleWrapperProps> = (props: IArticleWrapperProps) => {
  const {isLogged, onClick, url, children} = props;
  if (isLogged) {
    return (
      <a href={url} target='_blank' rel="noreferrer">
        {children}
      </a>
    );
  }
  return (
    <div onClick={onClick}>
      {children}
    </div>
  );
}

const SingleArticleComponent: React.FC<IArticlesProps> = (props: IArticlesProps) => {
  const { isLogged, data, onClick } = props;

  return (
    <ArticleWrapper isLogged={isLogged!} url={data.url} onClick={onClick}>
      <div className="single-article">
        <h3>{data.title}</h3>
        <p>{data.abstract}</p>
        <div className="single-article__foot">
          <p>{data.byline}</p>
          <p>{data.published_date}</p>
        </div>
      </div>
    </ArticleWrapper>
  );
}

const mapState = (state: AppState): IStateProps => ({
  isLogged: !state.firebase.auth.isEmpty
})

export const SingleArticle: React.FC<IArticlesProps> = connect(mapState)(SingleArticleComponent);