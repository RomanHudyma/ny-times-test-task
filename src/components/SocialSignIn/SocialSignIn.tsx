import React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../store/store';
import { AppActions } from '../../store/actionTypes/actionTypes';
import { signInWithGoogle, signInWithFacebook } from '../../store/actions/authActions';
import googleLogo from '../../assets/images/google-logo.svg';
import facebookLogo from '../../assets/images/facebook-logo.svg';
import './SocialSignIn.scss';

interface ISocialSignInProps {
  signInWithGoogle?: () => void,
  signInWithFacebook?: () => void
}

const SocialSignInComponent: React.FC<ISocialSignInProps> = (props: ISocialSignInProps) => {
  const { signInWithFacebook, signInWithGoogle } = props;

  return (
    <div className="social-sign-in">
      <button type="button" className="sign-in-button" onClick={signInWithGoogle}>
        <span className="sign-in-button__icon">
          <img src={googleLogo} alt="google"/>
        </span>
        <span className="sign-in-button__text">Sign in with Google</span>
      </button>
      <button type="button" className="sign-in-button" onClick={signInWithFacebook}>
        <span className="sign-in-button__icon">
          <img src={facebookLogo} alt="facebook"/>
        </span>
        <span className="sign-in-button__text">Sign in with Facebook</span>
      </button>
    </div>
  );
}

const mapDispatch = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => {
  return {
    signInWithGoogle: () => {dispatch(signInWithGoogle())},
    signInWithFacebook: () => {dispatch(signInWithFacebook())},
  };
}

export const SocialSignIn: React.FC<ISocialSignInProps> = connect(null, mapDispatch)(SocialSignInComponent);