import React, { useEffect, useState} from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { AppState } from '../../store/store';
import './UserPhoto.scss';

interface IUserPhotoProps {
  email?: string | null,
  photoURL?: string | null,
  className?: string
}

const UserPhotoComponent: React.FC<IUserPhotoProps> = (props: IUserPhotoProps) => {
  const [mailShortcut, setMailShortcut] = useState('');
  const { photoURL, email, className } = props;

  useEffect(() => {
    if (email) {
      setMailShortcut(email.substring(0, 2));
    }
  }, [email]);

  return (
    <div className={classnames("user-photo", className, {"user-photo--no-image": !photoURL})}>
      {!photoURL && <span>{mailShortcut}</span>}
      {photoURL && <img src={photoURL} alt='user'/>}
    </div>
  );
}

const mapState = (state: AppState): IUserPhotoProps => ({
  email: state.firebase.auth.email,
  photoURL: state.firebase.auth.photoURL
})

export const UserPhoto: React.FC<IUserPhotoProps> = connect(mapState)(UserPhotoComponent);