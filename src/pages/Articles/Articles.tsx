import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../store/store';
import { AppActions } from '../../store/actionTypes/actionTypes';
import { getArticles } from '../../store/actions/articlesActions';
import { SingleArticle } from '../../components/SingleArticle';
import { ErrorModal } from '../../components/ErrorModal';
import './Articles.scss';

interface IParentProps {}
interface IStateProps {
  isLogged?: boolean,
  articles: Array<{[key: string]: any}>
}
interface IDispatchProps {
  getData: () => void
}

type IArticlesProps = IParentProps & IStateProps & IDispatchProps;

const ArticlesComponent: React.FC<IArticlesProps> = (props: IArticlesProps) => {
  const { isLogged, getData, articles } = props;
  const [modalOpen, setModalOpen] = useState(false);

  const toggleModal = ():void => {
    setModalOpen(!modalOpen);
  }

  useEffect(() => {
    getData();   
  }, [getData])

  return (
    <>
    <div className="articles">
      <h1>Most Popular Articles from NY Times</h1>
      <div className="articles__wrapper">
        {articles.map((item) => {
          return <SingleArticle data={item} onClick={toggleModal} key={item.id}/>
        })}
      </div>
    </div>
    {!isLogged && modalOpen && <ErrorModal onClick={toggleModal} />}
    </>
  );
}

const mapState = (state: AppState): IStateProps => ({
  isLogged: !state.firebase.auth.isEmpty,
  articles: state.articles.articles
})

const mapDispatch = (dispatch: ThunkDispatch<AppState, {}, AppActions>): IDispatchProps => {
  return {
    getData: () => {dispatch(getArticles())}
  };
}

export const Articles: React.FC<IArticlesProps> = connect(mapState, mapDispatch)(ArticlesComponent);