import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { UserPhoto } from '../../components/UserPhoto';
import { AppState } from '../../store/store';
import './Profile.scss';

interface IProfileProps {
  username?: string | null,
  email?: string | null,
  redirect: string | null
}

const ProfileComponent: React.FC<IProfileProps> = (props: IProfileProps) => {

  const { username, email, redirect } = props;

  if (!redirect) {
    return <Redirect to='/' />
  }

  return (
    <div className="profile">
      <UserPhoto className="user-photo--profile"/>
      <div className="profile__creds">
        <p><span>Name:</span> {username ? username : 'Not provided.'}</p>
        <p><span>Email:</span> {email ? email : 'Not provided.'}</p>
      </div>
    </div>
  );
}

const mapState = (state: AppState): IProfileProps => ({
  username: state.firebase.auth.displayName,
  email: state.firebase.auth.email,
  redirect: state.auth.redirect
})

export const Profile: React.FC<IProfileProps> = connect(mapState)(ProfileComponent);