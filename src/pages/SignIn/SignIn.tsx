import React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { NavLink, Redirect } from 'react-router-dom';
import { AuthForm } from '../../components/AuthForm';
import { signIn, ICreds } from '../../store/actions/authActions';
import { AppState } from '../../store/store';
import { AppActions } from '../../store/actionTypes/actionTypes';
import { Button } from '../../components/Button';
import { SocialSignIn } from '../../components/SocialSignIn';


interface IStateProps {
  redirect: string | null
}
interface IDispatchProps {
  signIn: (creds: ICreds) => void,
}

type ISignInProps = IStateProps & IDispatchProps;

const SignInComponent: React.FC<ISignInProps> = (props: ISignInProps) => {
  const {signIn, redirect } = props;

  const handleFormSuccessSubmit = (email: string, password: string) => {
    signIn({email, password});
  }

  if (redirect) {
    return <Redirect to={redirect} />
  }

  return (
    <div className='sign-up'>
      <h1>Sign In</h1>
      <AuthForm onSuccessSubmit={handleFormSuccessSubmit}/>
      <div className="sign-up__or">
        <span>or</span>
        <NavLink to='/sign-up'>
          <Button label='Sign Up'/>
        </NavLink>
      </div>
      <SocialSignIn />
    </div>
  );
}

const mapState = (state: AppState): IStateProps => ({
  redirect: state.auth.redirect
});

const mapDispatch = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => {
  return {
    signIn: (creds: ICreds) => {dispatch(signIn(creds))}
  };
}

export const SignIn: React.FC<ISignInProps> = connect(mapState, mapDispatch)(SignInComponent);