import React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { NavLink, Redirect } from 'react-router-dom'; 
import { AuthForm } from '../../components/AuthForm';
import { signUp, ICreds } from '../../store/actions/authActions';
import { AppState } from '../../store/store';
import { AppActions } from '../../store/actionTypes/actionTypes';
import { Button } from '../../components/Button'; 
import { SocialSignIn } from '../../components/SocialSignIn';
import './SignUp.scss';

interface IStateProps {
  redirect: string | null
}
interface IDispatchProps {
  signUp: (creds: ICreds) => void,
}

type ISignUpProps = IStateProps & IDispatchProps;

const SignUpComponent: React.FC<ISignUpProps> = (props: ISignUpProps) => {
  const { signUp, redirect } = props;

  const handleFormSuccessSubmit = (email: string, password: string) => {
    signUp({email, password});
  }

  if (redirect) {
    return <Redirect to={redirect} />
  }


  return (
    <div className='sign-up'>
      <h1>Sign Up</h1>
      <AuthForm onSuccessSubmit={handleFormSuccessSubmit}/>
      <div className="sign-up__or">
        <span>or</span>
        <NavLink to='/sign-in'>
          <Button label='Sign In'/>
        </NavLink>
      </div>
      <SocialSignIn />
    </div>
  );
}

const mapState = (state: AppState): IStateProps => ({
  redirect: state.auth.redirect
});

const mapDispatch = (dispatch: ThunkDispatch<AppState, {}, AppActions>) => {
  return {
    signUp: (creds: ICreds) => {dispatch(signUp(creds))}
  };
}

export const SignUp: React.FC<ISignUpProps> = connect(mapState, mapDispatch)(SignUpComponent);