import { authActionTypes } from './authActionTypes';
import { articlesActionTypes } from './articlesActionTypes';

export type AppActions = authActionTypes | articlesActionTypes;