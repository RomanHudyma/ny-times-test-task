export const GET_ARTICLES = 'GET_ARTICLES';
export const GET_ARTICLES_ERROR = 'GET_ARTICLES_ERROR';

export interface getArticlesAction {
  type: typeof GET_ARTICLES,
  articles: Array<{}>
}

export interface getArticlesErrorAction {
  type: typeof GET_ARTICLES_ERROR,
  error: string
}

export type articlesActionTypes = getArticlesAction | getArticlesErrorAction;