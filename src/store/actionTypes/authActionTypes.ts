export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_REJECT = 'LOGIN_REJECT';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_REJECT = 'SIGNUP_REJECT';
export const SIGNOUT_SUCCESS = 'SIGNOUT_SUCCESS';

export interface loginSuccessAction {
  type: typeof LOGIN_SUCCESS,
  redirect: string | null
}

export interface loginRejectAction {
  type: typeof LOGIN_REJECT,
  errorMessage: string,
}

export interface signUpSuccessAction {
  type: typeof SIGNUP_SUCCESS,
  redirect: string | null
}

export interface signUpRejectAction {
  type: typeof SIGNUP_REJECT,
  errorMessage: string,
}

export interface signOutSuccessAction {
  type: typeof SIGNOUT_SUCCESS,
  redirect: string | null
}

export type authActionTypes = loginSuccessAction | loginRejectAction | signUpSuccessAction | signUpRejectAction | signOutSuccessAction;