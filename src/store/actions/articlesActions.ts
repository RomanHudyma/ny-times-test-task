import { Dispatch } from 'redux';
import { AppState } from '../store';
import { GET_ARTICLES_ERROR, GET_ARTICLES } from '../actionTypes/articlesActionTypes';
import { AppActions } from '../actionTypes/actionTypes';

export const getArticles = () => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState) => {
    fetch('https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=c641XBgSLuoupihyGfk1f0g2lPDoCEBV')
    .then((res) => {
      return res.json();  
    })
    .then((data) => {
      dispatch({type: GET_ARTICLES, articles: [...data.results]});
    })
    .catch((err) => {
      dispatch({type: GET_ARTICLES_ERROR, error: err})
    })
  }
}