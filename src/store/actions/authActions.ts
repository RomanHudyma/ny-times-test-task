import { Dispatch } from 'redux';
import { AppState } from '../store';
import {LOGIN_REJECT, LOGIN_SUCCESS, SIGNOUT_SUCCESS, SIGNUP_REJECT, SIGNUP_SUCCESS} from '../actionTypes/authActionTypes';
import { AppActions } from '../actionTypes/actionTypes';

export interface ICreds {
  email: string,
  password: string
}

export const signIn = (creds: ICreds) => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState, { getFirebase }: any) => {
    const firebase = getFirebase();
    firebase.auth().signInWithEmailAndPassword(creds.email, creds.password)
    .then(
      () => {dispatch({type: LOGIN_SUCCESS, redirect: '/'})})
    .catch(
      (err: Error) => {dispatch({type: LOGIN_REJECT, errorMessage: err.message})}
    );
  }
}

export const signUp = (creds: ICreds) => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState, { getFirebase }: any) => {
    const firebase = getFirebase();
    firebase.auth().createUserWithEmailAndPassword(creds.email, creds.password)
    .then(
      () => {dispatch({type: SIGNUP_SUCCESS, redirect: '/'})})
    .catch(
      (err: Error) => {dispatch({type: SIGNUP_REJECT, errorMessage: err.message})}
    );
  }
}

export const signOut = () => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState, { getFirebase }: any) => {
    const firebase = getFirebase();
    firebase.auth().signOut()
    .then(
      () => {dispatch({type: SIGNOUT_SUCCESS, redirect: null})})
    .catch(
      (err: Error) => {console.log(err)}
    );
  }
}

export const signInWithGoogle = () => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState, { getFirebase }: any) => {
    const firebase = getFirebase();
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider)
    .then(
      () => {dispatch({type: LOGIN_SUCCESS, redirect: '/'})})
    .catch(
      (err: Error) => {dispatch({type: LOGIN_REJECT, errorMessage: err.message})}
    );
  }
}

export const signInWithFacebook = () => {
  return (dispatch: Dispatch<AppActions>, getState: () => AppState, { getFirebase }: any) => {
    const firebase = getFirebase();
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider)
    .then(
      () => {dispatch({type: LOGIN_SUCCESS, redirect: '/'})})
    .catch(
      (err: Error) => {dispatch({type: LOGIN_REJECT, errorMessage: err.message})}
    );
  }
}