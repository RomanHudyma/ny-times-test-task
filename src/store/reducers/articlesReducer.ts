import { GET_ARTICLES, GET_ARTICLES_ERROR, articlesActionTypes } from '../actionTypes/articlesActionTypes';

interface IArticlesState {
  articlesError: string | null
  articles: Array<{}>
}

const initialState: IArticlesState = {
  articlesError: null,
  articles: []
}

export const articlesReducer = (state: IArticlesState = initialState, action: articlesActionTypes):IArticlesState => {
  switch (action.type) {
    case GET_ARTICLES:
      return {...state, articles: action.articles}
    case GET_ARTICLES_ERROR: 
      return {...state, articlesError: action.error}
    default:
      return {...state}
  }
}