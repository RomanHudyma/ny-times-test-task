import { LOGIN_REJECT, LOGIN_SUCCESS, SIGNUP_SUCCESS, SIGNUP_REJECT, SIGNOUT_SUCCESS, authActionTypes} from '../actionTypes/authActionTypes';

interface IAuthState {
  authError: string | null,
  redirect: string | null
}

const initialState: IAuthState = {
  authError: null,
  redirect: null
}

export const authReducer = (state:IAuthState = initialState, action: authActionTypes):IAuthState => {
  switch (action.type) {
    case LOGIN_SUCCESS:
    case SIGNUP_SUCCESS:
    case SIGNOUT_SUCCESS:
      return { ...state, redirect: action.redirect}
    case LOGIN_REJECT:
    case SIGNUP_REJECT:
      return { ...state, authError: action.errorMessage }
    default:
      return state;
  }
}