import { createStore, combineReducers, applyMiddleware, Action, compose } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import { AppActions } from './actionTypes/actionTypes';
import { firebaseReducer, getFirebase } from 'react-redux-firebase';
import { firestoreReducer, getFirestore } from 'redux-firestore';
import { authReducer } from './reducers/authReducer';
import { articlesReducer } from './reducers/articlesReducer';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;


export const rootReducer = combineReducers({
  auth: authReducer,
  articles: articlesReducer,
  firebase: firebaseReducer,
  firestore: firestoreReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore<AppState, Action<any>, {}, {}>(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore}) as ThunkMiddleware<AppState, AppActions>)),
);